#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

import numpy as np

from get_input import get_input
boardingPasses = get_input(5)

def bsp_to_sid(bsp):
    rows = list(range(0, 128))
    cols = list(range(0, 8))
    
    for c in bsp[:7]:
        if c == "F":
            rows = rows[:int(len(rows)/2)]
        if c == "B":
            rows = rows[int(len(rows)/2):]
    
    for c in bsp[7:]:
        if c == "L":
            cols = cols[:int(len(cols)/2)]
        if c == "R":
            cols = cols[int(len(cols)/2):]

    return ((rows[0] * 8) + cols[0])

def solve_puzzle():
    max_id = max(bsp_to_sid(bsp) for bsp in boardingPasses)
    return max_id

def solve_bonus():
    boardingids = sorted(bsp_to_sid(bsp) for bsp in boardingPasses)
    allboardingids = set(range(boardingids[0], boardingids[len(boardingids)-1]))
    boardingids = set(boardingids)
    return sorted(list(allboardingids-boardingids))[0]

if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle()))
    print("Bonus answer: " + str(solve_bonus()))
