#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

from get_input import get_input

def solve_puzzle():
    input_sorted = sorted(map(float, get_input(1)))
    for i in range(0, len(input_sorted)-1):
        for j in range(len(input_sorted)-1, 0, -1):
            if input_sorted[i] != input_sorted[j] and input_sorted[i] + input_sorted[j] == 2020:
                return int(input_sorted[i] * input_sorted[j])

def solve_bonus():
    input_sorted = sorted(map(float, get_input(1)))
    for i in range(0, len(input_sorted)-1):
        for j in range(0, len(input_sorted)-1):
            for k in range(0, len(input_sorted)-1):
                if input_sorted[i] != input_sorted[j] and input_sorted[i] != input_sorted[k] and input_sorted[k] != input_sorted[j] and input_sorted[i] + input_sorted[j] + input_sorted[k] == 2020:
                    return int(input_sorted[i] * input_sorted[j] * input_sorted[k])

if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle()))
    print("Bonus answer: " + str(solve_bonus()))
