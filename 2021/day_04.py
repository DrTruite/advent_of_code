#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

from parse import *
import string
import re

# Fonction perso pour charger les inputs
from get_input import get_input, fix_input
raw_passports = get_input(4)

# Ancienne fonction de check strict de passport pour la partie 2
def check_individual_passport(passport):
    is_valid = True
    
    byr = search("byr:{:d}", passport)[0]
    iyr = search("iyr:{:d}", passport)[0]
    eyr = search("eyr:{:d}", passport)[0]
    hgt = search("hgt:{:w}", passport)[0]
    hcl = search("hcl:{:S}", passport)[0]
    ecl = search("ecl:{:S}", passport)[0]
    pid = search("pid:{:S}", passport)[0]
    
    if byr<1920 or byr>2002 : is_valid = False
    if iyr<2010 or iyr>2020 : is_valid = False
    if eyr<2020 or eyr>2030 : is_valid = False
    if hgt[-2:] == "cm" and (int(hgt[:-2])<150 or int(hgt[:-2])>193) : is_valid = False
    elif hgt[-2:] == "in" and (int(hgt[:-2])<59 or int(hgt[:-2])>76) : is_valid = False
    elif hgt[-2:] != "cm" and hgt[-2:] != "in" : is_valid = False
    if hcl[0] != "#" or len(hcl) != 7 or any(symbol not in string.hexdigits for symbol in hcl[1:]) : is_valid = False
    if ecl not in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"] : is_valid = False
    if len(pid) != 9 or any(symbole not in string.digits for symbole in pid) : is_valid = False
    
    return is_valid

# Partie 1
def solve_puzzle():
    passports = fix_input(raw_passports)
    number_of_valid_passports = 0
    fields = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"]
    for passp in passports:
        if (all(f in passp for f in fields)):
            number_of_valid_passports += 1
    return number_of_valid_passports

# Partie 2, ancienne methode
def solve_bonus():
    passports = fix_input(raw_passports)
    number_of_valid_passports = 0
    fields = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"]
    for passp in passports:
        if (all(f in passp for f in fields)):
            if check_individual_passport(passp) : 
                number_of_valid_passports += 1
                print(passp)
    return number_of_valid_passports

# Verifier qu'un passeport a toute les entrées necessaire (NOUVEAU)
def has_required_keys(passport):
    if all(key in passport for key in ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]):
        return True
    else :
        return False

# Nouvelle fonction de check strict de passeport
def is_valid_passeport(passport):
    is_valid = True
    is_valid = is_valid and (int(passport["byr"])>=1920 and int(passport["byr"])<=2002)
    is_valid = is_valid and (int(passport["iyr"])>=2010 and int(passport["iyr"])<=2020)
    is_valid = is_valid and (int(passport["eyr"])>=2020 and int(passport["eyr"])<=2030)
    
    reg = re.findall('(\d+)(cm|in)', passport["hgt"])
    if len(reg) != 0:
        height, unit = reg[0]
        if unit == "cm":
            is_valid = is_valid and (int(height) >= 150 and int(height) <= 193)
        elif unit == "in":
            is_valid = is_valid and (int(height) >= 59 and int(height) <= 76)
        else:
            is_valid = False
    else :
        is_valid = False
        
    is_valid = is_valid and (re.match('#[0-9a-f]{6}', passport["hcl"]))
    is_valid = is_valid and (re.match('amb|blu|brn|gry|grn|hzl|oth', passport["ecl"]))
    is_valid = is_valid and (re.match('\d{9}$', passport["pid"]))
    
    return is_valid

# Partie 2, nouvelle methode
def solve_bonus_two():
    passports = fix_input(raw_passports)
    passports_dicts = []
    for passp in passports:
        passp_entry = {}
        entries = passp.split()
        for entry in entries:
            (key, value) = entry.split(":")
            passp_entry[key] = value
        passports_dicts.append(passp_entry)
    
    passports_dicts = list(filter(has_required_keys, passports_dicts))
    passports_dicts = list(filter(is_valid_passeport, passports_dicts))
    return len(passports_dicts)

# Si le code est appellé depuis la ligne de commande
if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle()))
    print("Bonus answer: " + str(solve_bonus_two()))
