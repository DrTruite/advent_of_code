#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

from get_input import get_input, fix_input
answers = fix_input(get_input(6))

def solve_puzzle():
    sum_of_answers = 0
    for grp in answers:
        clean_grp = grp.replace(" ", "")
        sum_of_answers += len(set(clean_grp))
    return sum_of_answers

def solve_bonus():
    sum_of_answers = 0
    for grp in answers:
        clean_grp = grp.split()
        grp_set = [set(ans) for ans in clean_grp]
        sum_of_answers += len(set.intersection(*grp_set))
    return sum_of_answers

if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle()))
    print("Bonus answer: " + str(solve_bonus()))
