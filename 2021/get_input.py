#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

# Read input file and return as a string
def get_input(day):
    input_filename = "inputs/day_" + str(day) + "_input.txt"
    with open(input_filename) as f:
        content = f.read().splitlines()
        return content

#put each paragraphs in a single line
def fix_input(raw):
    clean_input = [""]
    for line in raw:
        if line != "":
            clean_input[len(clean_input) -1] += (line + " ")
        else:
            clean_input.append("")
    return clean_input
