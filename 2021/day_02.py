#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

from get_input import get_input
from parse import *

def check_password_validity(password_line):
    pp = parse("{}-{} {}: {}", password_line)
    number_of_occurence = 0
    for letter in pp[3]:
        if letter == pp[2]:
            number_of_occurence += 1
    if number_of_occurence >= int(pp[0]) and number_of_occurence <= int(pp[1]):
        return True
    else:
        return False

def check_bonus_validity(password_line):
    pp = parse("{}-{} {}: {}", password_line)
    result = -1
    if pp[3][int(pp[0])-1] == pp[2]:
        result *= -1
    if pp[3][int(pp[1])-1] == pp[2]:
        result *= -1
        
    if result == 1:
        return True
    else:
        return False

def solve_puzzle():
    number_of_valid_pswd = 0
    for passw in get_input(2):
        if check_password_validity(passw):
            number_of_valid_pswd += 1
    return number_of_valid_pswd
    
def solve_bonus():
    number_of_valid_pswd = 0
    for passw in get_input(2):
        if check_bonus_validity(passw):
            number_of_valid_pswd += 1
    return number_of_valid_pswd

if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle()))
    print("Bonus answer: " + str(solve_bonus()))
