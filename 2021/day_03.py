#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

from get_input import get_input
forest_map = get_input(3)

def solve_puzzle(right, down):
    number_of_trees_encountered = 0
    for i in range(0, len(forest_map), down):
        if forest_map[i][int(i*right/down)%len(forest_map[0])] == "#":
            number_of_trees_encountered += 1
    return number_of_trees_encountered

if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle(3, 1)))
    bonus_answer = solve_puzzle(1, 1) * solve_puzzle(3, 1) * solve_puzzle(5, 1) * solve_puzzle(7, 1) * solve_puzzle(1, 2)
    print("Bonus answer : " + str(bonus_answer))
