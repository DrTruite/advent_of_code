#!/usr/bin/python3
# -*-coding:Utf-8 -*
# Généralement sous Linux ou Mac

from get_input import get_input

def get_rules():
    rules = {}
    r = [line.replace(" bags", "")
         .replace(" bag", "")
         .split(" contain ") for line in get_input(7)]
    for elem in r:
        rules[elem[0]] = elem[1].strip(".").split(", ")
    return rules

def solve_puzzle():
    rules = get_rules()
    checked_colours = []
    colours_to_check = ["shiny gold"]
    number_of_colours = 0
    
    while len(colours_to_check) > 0 :
        for rule in rules:
            for r in rules[rule]:
                if r.find(colours_to_check[0])>0 and not (rule in checked_colours) and not (rule in colours_to_check) :
                    colours_to_check.append(rule)
                    number_of_colours += 1
        checked_colours.append(colours_to_check[0])
        colours_to_check.pop(0)
    return number_of_colours

def solve_bonus():
    number_of_bags = 1
    colours_to_check = ["shiny gold"]
    
    rules = get_rules()
    for rule in rules[colours_to_check[0]]:
        
        
    
    return 0

if __name__ == "__main__":
    print("Puzzle answer: " + str(solve_puzzle()))
    print("Bonus answer: " + str(solve_bonus()))
